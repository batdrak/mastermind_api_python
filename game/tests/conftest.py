import pytest

from rest_framework.test import APIClient

from game.models import Game, Guess


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def game():
    return Game.objects.create(secret="BYBY")


@pytest.fixture
def game_with_guesses():
    game = Game.objects.create(secret="BYBY")
    Guess.objects.create(sequence="YYYY", game=game)
    Guess.objects.create(sequence="BBBB", game=game)
    Guess.objects.create(sequence="BBBY", game=game)
    return game


@pytest.fixture
def game_finished():
    return Game.objects.create(secret="BYBY", is_finished=True)
