import pytest
import uuid

from rest_framework import status


@pytest.mark.django_db
class TestCreateGame:
    """
    Test game creation
    """

    def test_create_game_default_successful(self, client):
        resp = client.post('http://localhost:8000/api/game', data={'secret': 'YYYY'}, format='json')
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.data['secret'] == 'YYYY'
        assert resp.data['max_moves'] == 10
        assert resp.data['is_finished'] is False
        assert type(resp.data['id']) == str

    def test_create_game_custom_max_moves(self, client):
        resp = client.post('http://localhost:8000/api/game', data={'secret': 'YYYY', 'max_moves': 20}, format='json')
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.data['secret'] == 'YYYY'
        assert resp.data['max_moves'] == 20
        assert resp.data['is_finished'] is False
        assert type(resp.data['id']) == str

    def test_create_game_secret_too_short(self, client):
        resp = client.post('http://localhost:8000/api/game', data={'secret': 'YYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_game_secret_too_long(self, client):
        resp = client.post('http://localhost:8000/api/game', data={'secret': 'YYYYYYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_game_without_data(self, client):
        resp = client.post('http://localhost:8000/api/game', format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
class TestGetGame:
    """
    Test retrieve game information
    """

    def test_get_game_info(self, game, client):
        resp = client.get('http://localhost:8000/api/game/{}'.format(game.id))
        assert resp.status_code == status.HTTP_200_OK
        assert resp.data['id'] == str(game.id)
        assert len(resp.data['guesses']) == 0

    def test_get_game_info_with_guesses(self, game_with_guesses, client):
        resp = client.get('http://localhost:8000/api/game/{}'.format(game_with_guesses.id))
        assert resp.status_code == status.HTTP_200_OK
        assert resp.data['id'] == str(game_with_guesses.id)
        assert len(resp.data['guesses']) == 3

    def test_get_game_not_exists(self, client):
        resp = client.get('http://localhost:8000/api/game/a1ef9d3e-8ed4-46ca-94ce-95516f4dd9be')
        assert resp.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
class TestCreateGuess:
    """
    Test guess creation
    """

    def test_create_guess_default_successful(self, game, client):
        resp = client.post('http://localhost:8000/api/game/{}/guess'.format(game.id), data={'sequence': 'YYYY'}, format='json')
        assert resp.status_code == status.HTTP_201_CREATED
        assert resp.data['sequence'] == 'YYYY'
        assert type(resp.data['close']) == int
        assert type(resp.data['exact']) == int
        assert type(resp.data['id']) == str
        assert type(resp.data['game']) == uuid.UUID

    def test_create_guess_in_not_existing_game(self, game, client):
        resp = client.post('http://localhost:8000/api/game/a1ef9d3e-8ed4-46ca-94ce-95516f4dd9be/guess', data={'sequence': 'YYYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_guess_with_game_finished(self, game_finished, client):
        resp = client.post('http://localhost:8000/api/game/{}/guess'.format(game_finished.id), data={'sequence': 'YYYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_guess_sequence_too_short(self, game, client):
        resp = client.post('http://localhost:8000/api/game/{}/guess'.format(game.id), data={'sequence': 'YYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_guess_sequence_too_long(self, game, client):
        resp = client.post('http://localhost:8000/api/game/{}/guess'.format(game.id), data={'sequence': 'YYYYYYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_create_guess_with_different_length_for_sequence_and_secret(self, game, client):
        resp = client.post('http://localhost:8000/api/game/{}/guess'.format(game.id), data={'sequence': 'YYYYY'}, format='json')
        assert resp.status_code == status.HTTP_400_BAD_REQUEST
