from django.contrib import admin

from .models import Game, Guess


class GameAdmin(admin.ModelAdmin):
    """
    Admin class in order to interact with Django Admin feature
    """

    list_display = ('id', 'secret', 'max_moves', 'is_finished')

    class Meta:
        model = Game
        fields = '__all__'


class GuessAdmin(admin.ModelAdmin):
    """
    Admin class in order to interact with Django Admin feature
    """

    list_display = ('id', 'sequence', 'exact', 'close')
    list_filter = ('game',)

    class Meta:
        model = Guess
        fields = '__all__'


admin.site.register(Game, GameAdmin)
admin.site.register(Guess, GuessAdmin)
