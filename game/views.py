from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from rest_framework import status

from .serializers import GameSerializer, GuessSerializer
from .models import Game


class CreateGame(APIView):
    """
    View to create and retrieve a Mastermind Game.
    """
    def get_object(self, pk):
        try:
            return Game.objects.get(pk=pk)
        except Game.DoesNotExist:
            raise NotFound

    def get(self, request, game_id):
        game = self.get_object(game_id)
        serializer = GameSerializer(game)
        return Response(serializer.data)

    def post(self, request):
        serializer = GameSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CreateGuess(APIView):
    """
    View to create a new Mastermind Guess for an specific game.

    Returns feedback information about the guess code given by the codebreaker
    """

    def post(self, request, game_id):
        request.data['game'] = game_id
        serializer = GuessSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
