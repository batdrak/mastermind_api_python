import uuid

from django.db import models
from django.core.validators import RegexValidator

validator = RegexValidator(
    r'^[RGBYOP]{4,6}$',
    'The sequence must have between 4 and 6 pegs. Available colors are: '
    'R(Red) G(Green) B(Blue) Y(Yellow) O(Orange) P(Purple)'
)


class Game(models.Model):
    """
    Game model, defines the following fields:

    id: Game identifier
    secret: Determinate the hidden code that allows win the game
    max_moves: Number of attempts the user has to solve the fire
    is_finished: Determine the state of the game
    created_at: Date of creation
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    secret = models.CharField(max_length=6, blank=False, null=False, validators=[validator])
    max_moves = models.IntegerField(default=10)
    is_finished = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = "Game"
        verbose_name_plural = "Games"

    @property
    def secret_length(self):
        return len(self.secret)

    def __str__(self):
        return str(self.id)


class Guess(models.Model):
    """
    Guess model, defines the following fields:

    id: Guess identifier
    sequence: Sequence of codes given by the codebreaker
    exact: Number of pegs that match in color and position
    close: Number of pegs that match only in color
    game: relates the guess to an specific game
    created_at: Date of creation
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sequence = models.CharField(max_length=6, blank=False, null=False, validators=[validator])
    exact = models.IntegerField(default=0)
    close = models.IntegerField(default=0)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='guesses')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = "Guess"
        verbose_name_plural = "Guesses"

    def save(self, *args, **kwargs):
        """
        Overwrite in order to calculates if the game has finished or not when save a Guess
        """

        super(Guess, self).save(*args, **kwargs)
        if self.game.guesses.count() == self.game.max_moves - 1:
            self.game.is_finished = True
            self.game.save()

    @property
    def sequence_length(self):
        return len(self.sequence)

    def __str__(self):
        return str(self.game) + ' number:' + str(self.id)
