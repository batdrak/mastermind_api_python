import collections

from rest_framework.exceptions import ValidationError
from rest_framework import serializers

from .models import Game, Guess


def calculate_feedback(sequence, secret):
    """
    Computes the feedback given an specific secret and sequence codes

    :return: Number of successful pegs and pegs with correct color but in a wrong position
    """
    # TODO: Fix bug ans test it
    unique_colors = collections.Counter(secret)
    exact = 0
    close = 0
    for seq, sec in zip(sequence, secret):
        if seq == sec:
            exact += 1
        elif seq in secret and unique_colors[seq] > 0:
            unique_colors[seq] = unique_colors[seq] - 1
            close += 1

    return exact, close


class GuessSerializer(serializers.ModelSerializer):
    """
    Model serializer of a guess
    """

    def create(self, validated_data):
        sequence = validated_data.get('sequence')
        game = validated_data.get('game')
        if len(sequence) != game.secret_length:
            raise ValidationError("Guess code has more or less pegs than allowed.")
        if game.is_finished:
            raise ValidationError("The match is over")

        exact, close = calculate_feedback(sequence, game.secret)
        validated_data["exact"] = exact
        validated_data["close"] = close
        return Guess.objects.create(**validated_data)

    class Meta:
        model = Guess
        fields = ('id', 'sequence', 'exact', 'close', 'game', 'created_at')
        read_only_fields = ('id', 'exact', 'close', 'created_at',)


class GameSerializer(serializers.ModelSerializer):
    """
    Model serializer of a game
    """

    guesses = GuessSerializer(many=True, read_only=True)

    class Meta:
        model = Game
        fields = ('id', 'secret', 'max_moves', 'guesses', 'is_finished', 'created_at')
        read_only_fields = ('id', 'is_finished', 'created_at',)
