"""
Game app URL Configuration
"""
from django.urls import path

from .views import CreateGame, CreateGuess

urlpatterns = [
    path('game', CreateGame.as_view(), name='create-game'),
    path('game/<str:game_id>', CreateGame.as_view(), name='game-detail'),
    path('game/<str:game_id>/guess', CreateGuess.as_view(), name='create-guess'),
]
