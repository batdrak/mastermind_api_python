# Mastermind API

## 1 Introduction
This API allows to take the codemaker role in the Mastermind game. The user has the ability to create games at will through HTTP requests.

Codemaker restrictions:
- Allowed pegs between 4 and 6.
- Only 6 color are available:
R(Red ![#f03c15](https://placehold.it/15/f03c15/000000?text=+)), 
G(Green  ![#2bcc12](https://placehold.it/15/2bcc12/000000?text=+)), 
B(Blue  ![#1168f4](https://placehold.it/15/1168f4/000000?text=+)), 
Y(Yellow  ![#f2e71d](https://placehold.it/15/f2e71d/000000?text=+)), 
O(Orange  ![#f2a60e](https://placehold.it/15/f2a60e/000000?text=+)), 
P(Purple ![#9c79d1](https://placehold.it/15/9c79d1/000000?text=+)).

Rest API build with Python 3.7 and Django 2.2.

## 2 Setup
Run the Docker containers with docker-compose:

`$ docker-compose up`

## 3 REST Endpoints
* `/game` [**POST**] - Creates a new game
* `/game/{id}/guess` [**POST**] - Creates nes Guess for an specific game and returns feedback
* `/game/{id}` [**GET**] - Gets info of an specific game, including the code guesses historical feedback

## 4 Tests
To execute tests run:

`$ docker-compose exec api pytest`
